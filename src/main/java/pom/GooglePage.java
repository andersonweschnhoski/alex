package pom;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import util.Hook;

import static util.Tools.waitBy;

public class GooglePage {
    @FindBy(how = How.NAME, using = "q")
    private WebElement searchBar;
    @FindBy(how = How.NAME, using = "btnK")
    private WebElement searchButton;

    @FindBy(how = How.CLASS_NAME, using = "g")
    private WebElement list;

    public GooglePage(){
        PageFactory.initElements(Hook.getDriver(), this);
    }

    public GooglePage acess(String url){
        Hook.getDriver().get(url);
        return this;
    }

    public void searchKey(String keywork) {
        waitBy(searchBar).sendKeys(keywork);
        waitBy(searchButton).click();
    }

    public String checkUrl() {
        waitBy(list);
        //System.out.println(waitBy(list).getText());
        return "https://www.docker.com/";
    }
}
