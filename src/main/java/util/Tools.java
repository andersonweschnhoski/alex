package util;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

public class Tools {
    public static WebElement waitBy
            (WebElement element) {
        try {
            new WebDriverWait(Hook.driver,
                    40)
                    .until(
                     ExpectedConditions.
                      visibilityOf(element));
            screenshot();
        } catch (Exception e) {
            System.err.println(
              element + " Não encontrado....");
        }
        return element;
    }


    public static void screenshot(){
        File scrFile = ((TakesScreenshot)Hook.driver).getScreenshotAs(OutputType.FILE);
        // Now you can do whatever you need to do with it, for example copy somewhere
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            FileUtils.copyFile(scrFile, new File(timestamp.getTime()+"screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
