package util;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Hook {
    protected static WebDriver driver;

    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions op = new ChromeOptions();
        op.addArguments("--headless");
        driver = new ChromeDriver(op);

    }
    @After
    public void after(){
        driver.quit();
    }

    public static WebDriver getDriver(){
        return driver;
    }

}
